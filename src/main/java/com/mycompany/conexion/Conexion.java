/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexion;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
    
    // Atributos
    private static String url = "jdbc:mysql://127.0.0.1:3306/consultoriovirtualdb";
    private static String user = "root";
    private static String password = "123456789";

    // Constructor
    public Conexion(){
        // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto    
    }
    
    // Métodos
    
    public static Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection;
        connection = DriverManager.getConnection(url,user,password);
        System.out.println("Connection is Successful to the database "+url);
        return connection;         
    }
    
    public static ResultSet ejecutarConsulta(String sql) throws ClassNotFoundException, SQLException{        
        Connection conex = iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        if(sql.toUpperCase().startsWith("SELECT")){
            return statement.getResultSet();
        } else{
            statement.close(); //Cerramos 
        }
        return null;
    }
    
}