/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio_ciclo3;

import com.mycompany.conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Nataly
 */
public class Diagnostico {
    
    //Atributos
    //private int idUsuarioMedico;
    //private int idUsuarioPaciente;
    private int idAgenda;
    private String fechaDiagnostico;
    private String Diagnostico;
    private String observaciones;
    //private String fechaCreacion;
    //private String fechaModificacion;
    
    //Constructor
    public Diagnostico () {
        
    }
    
    public Diagnostico(int idUsuarioMedico, int idUsuarioPaciente, int idAgenda, String fechaDiagnostico, String Diagnostico, String observaciones, String fechaCreacion, String fechaModificacion) {
        //this.idUsuarioMedico = idUsuarioMedico;
        //this.idUsuarioPaciente = idUsuarioPaciente;
        this.idAgenda = idAgenda;
        this.fechaDiagnostico = fechaDiagnostico;
        this.Diagnostico = Diagnostico;
        this.observaciones = observaciones;
        //this.fechaCreacion = fechaCreacion;
        //this.fechaModificacion = fechaModificacion;
    }
    
    
    //MÉTODOS - CRUD
    //Setters - Getters
    /*
    public int getIdUsuarioMedico() {
        return idUsuarioMedico;
    }
    
    public void setIdUsuarioMedico(int idUsuarioMedico) {
        this.idUsuarioMedico = idUsuarioMedico;
    }

    public int getIdUsuarioPaciente() {
        return idUsuarioPaciente;
    }

    public void setIdUsuarioPaciente(int idUsuarioPaciente) {
        this.idUsuarioPaciente = idUsuarioPaciente;
    }
    */
    public int getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(int idAgenda) {
        this.idAgenda = idAgenda;
    }

    public String getFechaDiagnostico() {
        return fechaDiagnostico;
    }

    public void setFechaDiagnostico(String fechaDiagnostico) {
        this.fechaDiagnostico = fechaDiagnostico;
    }

    public String getDiagnostico() {
        return Diagnostico;
    }

    public void setDiagnostico(String Diagnostico) {
        this.Diagnostico = Diagnostico;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    /*
    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    */
    //CRUD
    //C-Create-Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //Conexion con = new Conexion(); // Instancia de la conexión
        String sql = "INSERT INTO usuarios(idAgenda, fechaDiagnostico, Diagnostico, observaciones) VALUES(" + getIdAgenda() 
                + "','" + getFechaDiagnostico() + "','" + getDiagnostico() + "','" + getObservaciones() + "','"  + "')";
        Conexion.ejecutarConsulta(sql);
        //boolean rs = statement.execute(sql);
        //statement.close();
        //conex.close();
    }
    
    //R-Read-Consultar
    public void consultar() throws SQLException, ClassNotFoundException{
        // CRUD - R  Read = Leer, Consultar
        String sql = " SELECT idAgenda, fechaDiagnostico, Diagnostico, observaciones FROM dignosticos WHERE idAgenda = '"+getIdAgenda()+"';";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        if(rs.next()){ // Hay un dato en resultset ?
            String nombre = rs.getString(2); // Obtener el dato de la columna 2
            this.setIdAgenda(rs.getInt("idAgenda"));
            this.setFechaDiagnostico(rs.getString("fechaDiagnostico"));
            this.setDiagnostico(rs.getString("Diagnostico"));
            this.setObservaciones(rs.getString("Observaciones"));
        }else{ // si no hay dato
            System.out.println("No se encontraron datos solicitados");
        }    
     
    }
    
    public String actualizar() throws SQLException, ClassNotFoundException {
        // CRUD - U  Update = Actualizar

        String sql = "UPDATE usuarios SET FechaDiagnostico = '"+getFechaDiagnostico()+"', Diagnostico = '"+getDiagnostico()+"', observaciones = '"+getObservaciones()+"' WHERE idAgenda = " + getIdAgenda() ;
        Conexion.ejecutarConsulta(sql);
        return sql;
    }
    
    public String borrar() throws ClassNotFoundException, SQLException {
        // CRUD - D  Delete = Borrar

        String sql = "DELETE FROM diagnosticos WHERE idAgenda = " + getIdAgenda();
        Conexion.ejecutarConsulta(sql);
        return sql;
    }
    
    
    
    

}
