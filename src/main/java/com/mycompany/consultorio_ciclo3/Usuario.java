/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio_ciclo3;

import com.mycompany.conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


/**
 *
 * @author May Perez
 */
public class Usuario {
    //Programacion oruientada a objetos

    //Atributos
    private int idusuarios;
    private String nombreUusario;
    private String tipoUsuario;
    private String cedula;
    private String fechaCreacion;
    private String fechaModificacion;
    private String fechaUltimoIngreso;
    private String direccion;
    private String localidad;
    private String ciudad;
    private String genero;
    private String fechaNacimiento;
    private String observaciones;
    private String correo;
    private String celular;
    private String contrasena;
    private String fraseRecordacion;

    //Cosntructor           
    public Usuario(int idusuarios, String nombreUusario, String tipoUsuario, String cedula, String fechaCreacion,
            String fechaModificacion, String fechaUltimoIngreso, String direccion, String localidad, String ciudad,
            String genero, String fechaNacimiento, String observaciones, String correo, String celular, String contrasena,
            String fraseRecordacion) {

        this.idusuarios = idusuarios;
        this.nombreUusario = nombreUusario;
        this.tipoUsuario = tipoUsuario;
        this.cedula = cedula;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.fechaUltimoIngreso = fechaUltimoIngreso;
        this.direccion = direccion;
        this.localidad = localidad;
        this.ciudad = ciudad;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.observaciones = observaciones;
        this.correo = correo;
        this.celular = celular;
        this.contrasena = contrasena;
        this.fraseRecordacion = fraseRecordacion;
  
    }
    public Usuario(){
    
    }
    
        //Metodos
    public int getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getNombreUusario() {
        return nombreUusario;
    }

    public void setNombreUusario(String nombreUusario) {
        this.nombreUusario = nombreUusario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaUltimoIngreso() {
        return fechaUltimoIngreso;
    }

    public void setFechaUltimoIngreso(String fechaUltimoIngreso) {
        this.fechaUltimoIngreso = fechaUltimoIngreso;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getFraseRecordacion() {
        return fraseRecordacion;
    }

    public void setFraseRecordacion(String fraseRecordacion) {
        this.fraseRecordacion = fraseRecordacion;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idusuarios = " + idusuarios
                + ",nombreUusario = " + nombreUusario
                + ",tipoUsuario =" + tipoUsuario
                + ",cedula =" + cedula
                + ",fechaCreacion =" + fechaCreacion
                + ",fechaModificacion =" + fechaModificacion
                + ",fechaUltimoIngreso =" + fechaUltimoIngreso
                + ",direccion =" + direccion
                + ",localidad =" + localidad
                + ",ciudad =" + ciudad
                + ",genero =" + genero
                + ",fechaNacimiento =" + fechaNacimiento
                + ",observaciones =" + observaciones
                + ",correo =" + correo
                + ",celular =" + celular
                + ",contrasena =" + contrasena
                + ",fraseRecordacion =" + fraseRecordacion + '}';

    }

    //CRUD
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD -C
        //tipo del dato (Conexion) y el nombre del dato (con)
        Conexion con = new Conexion(); // Instancia de la conexión
        String sql = "INSERT INTO usuarios(idusuarios,nombreUusario,tipoUsuario,cedula,fechaCreacion,fechaModificacion,fechaUltimoIngreso,direccion,localidad,ciudad,genero,fechaNacimiento,observaciones,correo,celular,contrasena,fraseRecordacion) VALUES(" + getIdusuarios()
                + ",'" + getNombreUusario() + "','" + getTipoUsuario() + "','" + getCedula() + "','" + getFechaCreacion()
                + "','" + getFechaModificacion() + "','" + getFechaUltimoIngreso() + "','" + getDireccion() + "','" + getLocalidad() 
                + "','" + getCiudad()+ "','" + getGenero() + "','" + getFechaNacimiento() + "','" + getObservaciones() + "','" + getCorreo() + "','" 
                + getCelular() + "','" + getContrasena()+ "','" + getFraseRecordacion() + "')";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        //boolean rs = statement.execute(sql);
        statement.close();
        conex.close();
    }
    
    public void consultar() throws SQLException, ClassNotFoundException{
        // CRUD - R  Read = Leer, Consultar
        Conexion con =  new Conexion();
        String sql = "SELECT idusuarios,nombreUusario,tipoUsuario,cedula,fechaCreacion,fechaModificacion,fechaUltimoIngreso,direccion,localidad,ciudad,genero,fechaNacimiento,observaciones,correo,celular,contrasena,fraseRecordacion = '"+getCedula()+"';";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next()){ // Hay un dato en resultset ?
            String nombre = rs.getString(2); // Obtener el dato de la columna 2
        }else{ // si no hay dato
            System.out.println("No se encontraron datos solicitados");
        }
        
    }
    
    public void actualizar() throws SQLException, ClassNotFoundException{
        // CRUD - U  Update = Actualizar
        Conexion con =  new Conexion();
        String sql = "UPDATE usuarios SET cedula ="+getIdusuarios()+",'" + getNombreUusario() + "','" + getTipoUsuario() 
                + "','" + getCedula() + "','" + getFechaCreacion()+ "','" + getFechaModificacion() + "','" + getFechaUltimoIngreso() 
                + "','" + getDireccion() + "','" + getLocalidad()+ "','" + getCiudad()+ "','" + getGenero() + "','" 
                + getFechaNacimiento() + "','"+ getObservaciones() + "','" + getCorreo() + "','" + getCelular() + "','" 
                + getContrasena()+ "','" + getFraseRecordacion() + " WHERE donde Cedula ='" + getCedula() +"';";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
    
    public void borrar() throws ClassNotFoundException, SQLException{
        // CRUD - D  Delete = Borrar
        Conexion con =  new Conexion();
        String sql = "DELETE FROM usuarios WHERE idusuarios ="+getIdusuarios()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    }
    
}
