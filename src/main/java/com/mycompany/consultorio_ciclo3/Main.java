/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio_ciclo3;

import com.mycompany.conexion.Conexion;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
* @author May Perez
 */
public class Main {
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException
    {
        System.out.println("##############");
        System.out.println("Bienvenido a su Consultorio Virtual");
        System.out.println("##############");
        
        Usuario u = new Usuario();
        u.setIdusuarios(1111111);
        u.setNombreUusario("Mayra");
        u.setTipoUsuario("a");
        u.setCedula("4444444");
        u.setFechaCreacion("2021-09-10 13:01:00");
        u.setFechaModificacion("15-09-21 13:01:00");
        u.setFechaUltimoIngreso("15-09-21");
        u.setDireccion("calle 11 # 22 - 50");
        u.setLocalidad("San Francisco");
        u.setCiudad("Bucaramanga");
        u.setGenero("F");
        u.setFechaNacimiento("1987-09-21");
        u.setObservaciones("Prueba");
        u.setCorreo("prueba@consultorio.com");
        u.setCelular("3142344567");
        u.setContrasena("A1123#");
        u.setFraseRecordacion("admin");
        
        System.out.println(u);
        u.guardar();
        ejecutar();      
                
       
    }
    
    public static void ejecutar() throws SQLException, ClassNotFoundException{
        
    }
          
}