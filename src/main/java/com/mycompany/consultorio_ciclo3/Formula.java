/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio_ciclo3;

import com.mycompany.conexion.Conexion;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author usuario
 */
public class Formula {
    private int idAgenda;
    private Date fechaFormula;
    private String formula;
    private String observaciones;

    public Formula(int idAgenda, Date fechaFormula, String formula, String observaciones) {
        this.idAgenda = idAgenda;
        this.fechaFormula = fechaFormula;
        this.formula = formula;
        this.observaciones = observaciones;
    }

    public int getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(int idAgenda) {
        this.idAgenda = idAgenda;
    }

    public Date getFechaFormula() {
        return fechaFormula;
    }

    public void setFechaFormula(Date fechaFormula) {
        this.fechaFormula = fechaFormula;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    

 
    public void guardar() throws SQLException, ClassNotFoundException {
    Conexion con = new Conexion(); //instancia de la conexion
    String sql = "INSERT INTO formulas(idAgenda, fechaFormula, formula, observaciones) "
            + "VALUES("+getIdAgenda()+",'"+getFechaFormula()+"','"+getFormula()+"','"+getObservaciones()+"')";
    Connection conex = con.iniciarConexion();
    Statement statement = conex.createStatement();
    statement.execute(sql);
        }
    
    public void consultar() throws SQLException, ClassNotFoundException{
    Conexion con = new Conexion(); 
    String sql = "SELECT idAgenda, fechaFormula, formula, observaciones FROM formulas WHERE idAgenda= "+this.getIdAgenda()+";";
    Connection conex = con.iniciarConexion();
    Statement statement = conex.createStatement();
    statement.execute(sql);
    }
    
    public void actualizar() throws SQLException, ClassNotFoundException{
    Conexion con = new Conexion();
    String sql = "UPDATE formulas SET IdAgenda="+getIdAgenda()+",fechaFormula="+getFechaFormula()+",formula="+getFormula()+",observaciones="+getObservaciones()+" WHERE idAgenda="+getIdAgenda()+";";
    Connection conex = con.iniciarConexion();
    Statement statement = conex.createStatement();
    statement.execute(sql);
    }
    
    public void borrar() throws ClassNotFoundException, SQLException{
    Conexion con = new Conexion();
    String sql = "DELETE FROM formulas WHERE idAgenda="+getIdAgenda()+";";
    Connection conex = con.iniciarConexion();
    Statement statement = conex.createStatement();
    statement.execute(sql);}
    
        @Override
    public String toString() {
        return "Formula{"+",idAgenda="+idAgenda+ ",fechaFormula="+fechaFormula+ ",formula="+formula+ 
                ",observaciones="+observaciones+'}';
  }
}

